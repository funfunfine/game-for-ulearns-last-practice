﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace YetAnotherAttemptToGame
{
    [TestFixture]
    class MovingObjectTest
    {
        [Test]
        public void Creation()
        {
            var player = new MovingObject(new Rectangle(10,10,6,5));
            Assert.AreEqual(new Rectangle(9,10,1,5), player.LeftScanner);
            Assert.AreEqual(new Rectangle(16,10,1,5), player.RightScanner);
            Assert.AreEqual(new Rectangle(10,9,6,1), player.TopScanner);
            Assert.AreEqual(new Rectangle(10,15,6,1), player.BottomScanner);
        }

        [TestCase(14,29)]
        [TestCase(13,29)]
        public void TestMovement(int h, int result)
        {
            var map = new Map(50,50, new Rectangle(0, 10, 10, 30), new Rectangle(0, 30, 40, 10));
            var player = new MovingObject(new Rectangle(10,15,5,h),map);
            player.UpdatePosition(15);
            Assert.AreEqual(result,player.MainRectangle.Bottom);

        }

        [Test]
        public void TestScanners()
        {
            var map = new Map(50,50);
            map.AddWalls(new Rectangle(0,30,50,20));
            var player = new Player(new Rectangle(0,20,15,10),map);
            Assert.AreEqual(new Tile[] { Tile.Wall, Tile.Wall }, player.BottomIntersectedTiles.Select(p=>map.Tiles[p.X,p.Y]).ToArray());
            map.AddSharper(new Point(0,3));
            Assert.IsTrue(player.IsOnSharper);
        }
    }



    [TestFixture]
    class PlayerTest
    {
        [TestCase()]
        public void Winner(int x, int y, int w, int h, bool result)
        {
            var map = new Map(50, 50, walls: new Rectangle(0, 30, 40, 10));

            var player = new Player(new Rectangle(10, 15, 5, h), map);

        }
    }
}

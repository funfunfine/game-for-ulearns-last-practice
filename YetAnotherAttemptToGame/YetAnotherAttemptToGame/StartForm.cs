﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YetAnotherAttemptToGame
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
            Levels.MouseEnter += SearchLevels;
            SearchLevels(null,null);
            Levels.Items.Add("map_1");
            Levels.SelectedItem = Levels.Items[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var testMap =
                BinarySerialization.ReadFromBinaryFile<Map>(
                    ConstructorForm.PathToLevels+Levels.SelectedItem.ToString());
            Constants.TileSize = 16;
            
            var player = new Player(testMap.PlayerStartRectangle, testMap);
            var model = new GameModel(1, testMap, player);
            var form = new GameForm(model);
            form.Show();
            
        }

        private void Constructor_Click(object sender, EventArgs e)
        {
            var width = int.Parse(GameWidth.Text);
            var height = int.Parse(GameHeight.Text);
            Constants.TileSize = int.Parse(TileSize.Text);
            var form = new ConstructorForm(width,height);
            form.Show();
            
        }

        private void SearchLevels(object sender, EventArgs e)
        {
            Levels.Items.Clear();
            var files =
                Directory.GetFiles(ConstructorForm.PathToLevels, "*", SearchOption.TopDirectoryOnly)
                         .Select(f=>f.Split('\\').Last())
                         .ToArray();
            Levels.Items.AddRange(files);
            Levels.SelectedItem = Levels.Items[0];

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Width_TextChanged(object sender, EventArgs e)
        {

        }

        private void TileSize_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}

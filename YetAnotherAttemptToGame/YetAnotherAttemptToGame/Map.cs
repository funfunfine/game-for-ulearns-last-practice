﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace YetAnotherAttemptToGame
{
    [Serializable]
    public class Map
    {
        private readonly Queue<Point> _darkQueue = new Queue<Point>();

        private readonly HashSet<Point> _darkTiles = new HashSet<Point>();
        public readonly Tile[,] Tiles;

        private int _darknessCount;
        private Point _darkSeed;

        public Rectangle PlayerStartRectangle = Rectangle.Empty;

        public Map(int w, int h, params Rectangle[] walls)
        {
            Tiles = new Tile[w / Constants.TileSize, h / Constants.TileSize];
            foreach (var rectangle in walls.ToTilePositions())
                Tiles[rectangle.X, rectangle.Y] = Tile.Wall;
            _darkQueue.Enqueue(_darkSeed);
        }

        public int Width => Tiles.GetLength(0) * Constants.TileSize;
        public int Height => Tiles.GetLength(1) * Constants.TileSize;

        private int TilesWidth => Tiles.GetLength(0);
        private int TilesHeight => Tiles.GetLength(1);

        public static Size TileSize => new Size(Constants.TileSize, Constants.TileSize);

        public IEnumerable<Rectangle> AllTiles
                                => TilesUnderRectangle(new Rectangle(0, 0, Width - 1, Height - 1))
                                        .Select(r => new Rectangle((r * Constants.TileSize).ToPoint(), TileSize));

        public void ClearDarkness()
        {
            foreach (var point in _darkTiles.Where(p=>Tiles[p.X,p.Y]==Tile.Dark))
            {
                Tiles[point.X, point.Y] = Tile.None;
            }
            _darkTiles.Clear();
            _darkQueue.Clear();
            _darkQueue.Enqueue(_darkSeed);
            _darkTiles.Add(_darkSeed);
            Tiles[_darkSeed.X, _darkSeed.Y] = Tile.Dark;

        }

        public void AddDarkSeed(Point point)
        {
            if (!TileInBounds(point.ToVector()))
                return;
            if (_darkTiles.Count>0)
                return;
            Tiles[point.X, point.Y] = Tile.Dark;
            _darkTiles.Add(point);
            _darkSeed = point;
            _darkQueue.Enqueue(_darkSeed);
        }

        public void ExpandDarkness(int speed)
        {


            for (int i = 0; i < speed; i++)
            {
                DarkIteration();
            }
                
           

        }

        private void DarkIteration()
        {
            if (_darkQueue.Count==0)
                return;       
            var darkPoint = _darkQueue.Dequeue();
            if (Tiles[darkPoint.X,darkPoint.Y]==Tile.Wall || Tiles[darkPoint.X, darkPoint.Y] == Tile.Win || Tiles[darkPoint.X, darkPoint.Y] == Tile.Sharp)
                return;
            Tiles[darkPoint.X, darkPoint.Y] = Tile.Dark;
            foreach (var localTile in GetLocalTiles(darkPoint).Where(p=>!_darkTiles.Contains(p)).Where(p=>Tiles[p.X,p.Y]==Tile.None))
            {
                _darkTiles.Add(localTile);
                _darkQueue.Enqueue(localTile);
            }
        }

        public bool AnyOtherTiles(Tile otherTile, Rectangle tile) 
            => GetLocalTiles(ConvertToTiles(tile.Location.ToVector()).ToPoint(), 1)
                                                    .Any(p => Tiles[p.X, p.Y] == otherTile);

        public void AddWalls(params Rectangle[] walls)
        {
            var tiles = walls.ToTilePositions();
            foreach (var rectangle in tiles)
                Tiles[rectangle.X, rectangle.Y] = Tile.Wall;
        }

        public void AddWall(Point point)
        {
            if (TileInBounds(point.ToVector()))
                Tiles[point.X, point.Y] = Tile.Wall;
        }

        public void AddSharper(Point point)
        {
            if (TileInBounds(point.ToVector()))
                Tiles[point.X, point.Y] = Tile.Sharp;
        }

        public void AddWinArea(Point point)
        {
            if (TileInBounds(point.ToVector()))
                Tiles[point.X, point.Y] = Tile.Win;
        }

        public void AddNone(Point point)
        {
            if (TileInBounds(point.ToVector()))
                Tiles[point.X, point.Y] = Tile.None;
            if (Tiles[point.X, point.Y] == Tile.Dark)
                _darkTiles.Remove(point);
        }

        public void AddBorder()
        {
            var walls = new[]
            {
                new Rectangle(5, 0, Width - 5, 5), new Rectangle(5, Height - 5, Width - 5, 5), new Rectangle(0, 0, 5, Height),
                new Rectangle(Width - 5, 0, 5, Height)
            };
            AddWalls(walls);
        }

        public IEnumerable<Rectangle> TileRectangles(Tile tile) => AllTiles.Select(r => r.Location.ToVector() / Constants.TileSize)
                                                                .Where(p => Tiles[p.X, p.Y] == tile)
                                                                .Select(p => new Rectangle((p * Constants.TileSize).ToPoint(), TileSize));

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (var y = 0; y < TilesHeight; y++)
            {
                sb.Append('\n');
                for (var x = 0; x < TilesWidth; x++)
                    sb.Append(Tiles[x, y] == Tile.None ? '_' : Tiles[x, y] == Tile.Wall ? 'W' : '#');
            }
            return sb.Remove(0, 1).ToString();

        }

        public IEnumerable<Vector> Intersections(Rectangle rectangle) => TilesUnderRectangle(rectangle).Where(p => Tiles[p.X, p.Y] != Tile.None);

        public bool AnyIntersection(Rectangle rectangle)
        {
            if (!(InBounds(rectangle.Location) && InBounds(rectangle.Location.ToVector() + rectangle.Size.ToVector())))
                return true;
            return Intersections(rectangle).Any();
        }

        private bool InBounds(Vector point) => InBounds(point.ToPoint());

        public bool InBounds(Point point) => point.X >= 0 && point.Y >= 0 && point.X < Width && point.Y < Height;

        private bool TileInBounds(Vector v) => v.X >= 0 && v.Y >= 0 && v.X < TilesWidth && v.Y < TilesHeight;

        private static int ConvertToTiles(int coordinate) => coordinate / Constants.TileSize;

        public static Vector ConvertToTiles(Vector point) => new Vector(ConvertToTiles(point.X), ConvertToTiles(point.Y));

        public static IEnumerable<Vector> TilesUnderRectangle(Rectangle rectangle)
        {
            var xMax = ConvertToTiles(rectangle.Right);
            var yMax = ConvertToTiles(rectangle.Bottom);
            for (var x = ConvertToTiles(rectangle.Left); x <= xMax; x++)
                for (var y = ConvertToTiles(rectangle.Top); y <= yMax; y++)
                    yield return new Vector(x, y);
        }

        public IEnumerable<Point> GetLocalTiles(Point point, int width)
        {
            if (width == 0)
                return new List<Point> { point };
            IEnumerable<Point> result = GetLocalTiles(point).ToList();
            for (var i = 0; i < width - 1; i++)
            {
                var otherLocals = new List<Point>();
                foreach (var localPoint in result)
                    otherLocals.AddRange(GetLocalTiles(localPoint).Where(p => !result.Contains(p)));
                result = result.Concat(otherLocals).Distinct();
            }

            return result.Distinct().Where(p => p != point);
        }

        private IEnumerable<Point> GetLocalTiles(Point point)
        {
            var shift = new[] { -1, 0, 1 };
            return shift.SelectMany(s => shift, (x, y) => new Vector(x, y))
                        .Where(p => (p.X == 0) ^ (p.Y == 0))
                        .Select(v => point.ToVector() + v)
                        .Where(TileInBounds)
                        .Select(v => v.ToPoint());
        }
    }

    public static class RectanglesExtensions
    {
        public static IEnumerable<Vector> ToTilePositions(this IEnumerable<Rectangle> rectangles)
                                                        => rectangles.Select(r => new Rectangle(r.Location, new Size(r.Width - 1, r.Height - 1)))
                                                                     .SelectMany(Map.TilesUnderRectangle);
    }

    public enum Tile
    {
        None = 0,
        Wall,
        Sharp,
        Win,
        Dark
    }
}

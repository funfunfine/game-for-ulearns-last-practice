﻿using System.Drawing;

namespace YetAnotherAttemptToGame
{
    public static class Constants
    {
        public static int TileSize = 10;
        public static int Gravity = 5;
        public static int JumpSpeed = 40;
        public static int HorizontalSpeed = 4;

        public static int HorizontalAcceleration = 2;
        public static Size IflateSize = new Size(2,2);
        public static readonly int DarknessSpeed = 3;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace YetAnotherAttemptToGame
{
     internal sealed class GameForm : Form
    {
        private static readonly string ImagesPath =Environment.CurrentDirectory+ @"\..\..\Images\";

        private readonly GameModel _game;

        private readonly HashSet<Keys> _keysPressed = new HashSet<Keys>();

        private readonly Lazy<Dictionary<Tile, Rectangle[]>> _tileArrays;

        private readonly Dictionary<Tile, List<Image>> _tileImages = new Dictionary<Tile, List<Image>>
        {
            [Tile.Wall] = new List<Image>(),
            [Tile.Sharp] = new List<Image>(),
            [Tile.Win] = new List<Image>(),
            [Tile.Dark] = new List<Image>()
        };

        private readonly Timer _timer = new Timer { Interval = 15 };

        private Dictionary<Keys, Action> _keyMovements;

        private Image _playerImage;

        private Image _trayImage;

        public GameForm(GameModel model)
        {
            _game = model;

            _tileArrays = new Lazy<Dictionary<Tile, Rectangle[]>>(TileArraysFactory);

            PrepareImages();


            InitializeKeyMovements();
            InitializeForm();
            _timer.Start();
        }

        private Dictionary<Tile, Rectangle[]> TileArraysFactory()
        {
            var result = new Dictionary<Tile, Rectangle[]>();
            for (var i = 0; i < 5; i++)
            {
                var tile = (Tile)i;
                result[tile] = _game.Map.TileRectangles(tile).ToArray();
            }
            return result;
        }

        private void PrepareImages()
        {
            _trayImage = Image.FromFile(ImagesPath + "tray.png");

            _playerImage = Image.FromFile(ImagesPath + "player.png");

            _tileImages[Tile.Wall].Add(Image.FromFile(ImagesPath + "wall.png"));
            _tileImages[Tile.Wall].Add(Image.FromFile(ImagesPath + "wall_edge.png"));

            _tileImages[Tile.Sharp].Add(Image.FromFile(ImagesPath + $"sharp.png"));
            _tileImages[Tile.Sharp].Add(Image.FromFile(ImagesPath + $"sharp.png"));


            _tileImages[Tile.Win].Add(Image.FromFile(ImagesPath + $"win.png"));
            _tileImages[Tile.Win].Add(Image.FromFile(ImagesPath + $"win_edge.png"));

            _tileImages[Tile.Dark].Add(Image.FromFile(ImagesPath + $"dark.png"));
            _tileImages[Tile.Dark].Add(Image.FromFile(ImagesPath + $"dark.png"));


        }

        private void InitializeForm()
        {
            _timer.Tick += TimerTick;
            Paint += DrawGame;
            ClientSize = new Size(_game.Map.Width, _game.Map.Height);
            DoubleBuffered = true;
            FormBorderStyle = FormBorderStyle.None;

        }

        private void InitializeKeyMovements()
        {
            _keyMovements = new Dictionary<Keys, Action>
            {
                [Keys.Space] = _game.Jump,
                [Keys.A] = _game.MoveLeft,
                [Keys.D] = _game.MoveRight,
                [Keys.R] = _game.Restart
            };
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.B ||e.KeyCode== Keys.Escape)
            {
                Close();
            }
            if (_keyMovements.ContainsKey(e.KeyCode))
                _keysPressed.Add(e.KeyCode);
        }

        protected override void OnKeyUp(KeyEventArgs e) => _keysPressed.Remove(e.KeyCode);

        private void DrawGame(object sender, PaintEventArgs e)
        {

            var g = e.Graphics;
            var shouldContinue = CheckGameState(g);
            if (!shouldContinue)
                return;
            Draw(g);

        }

        private bool CheckGameState(Graphics g)
        {
            switch (_game.State)
            {
                case GameModel.GameState.Won:
                    DrawWon(g);
                    return false;
                case GameModel.GameState.Lost:
                    DrawLost(g);
                    return false;
            }

            return true;
        }

        private void DrawLost(Graphics g)
        {
            var image = Image.FromFile(ImagesPath + "lose.jpg");
            g.DrawImage(image, ClientRectangle);
        }

        private void DrawWon(Graphics g)
        {
            var image = Image.FromFile(ImagesPath + "win.jpg");
            g.DrawImage(image, ClientRectangle);
        }

        private void DrawLostDebug(Graphics g)
        {
            g.FillRectangle(Brushes.Black, ClientRectangle);
        }

        private void DrawWonDebug(Graphics g)
        {
            g.FillRectangle(Brushes.Green, ClientRectangle);
        }

        private void DrawDebug(Graphics g)
        {
            g.DrawRectangles(Pens.Black, _game.Map.AllTiles.ToArray());

            DrawTilesDebug(g, Tile.Wall, Color.DarkSlateGray);
            DrawTilesDebug(g, Tile.Sharp, Color.Red);
            DrawTilesDebug(g, Tile.Win, Color.Green);

            var playerRectangles = Map.TilesUnderRectangle(_game.Player.MainRectangle)
                                      .Select(p => new Rectangle((p * Constants.TileSize).ToPoint(), Map.TileSize))
                                      .ToArray();
            g.FillRectangles(Brushes.Aquamarine, playerRectangles);

            if (_game.Tray.Count > 2)
                g.DrawCurve(new Pen(Color.DeepSkyBlue, 4), _game.Tray.ToArray());

            g.FillRectangle(Brushes.Blue, _game.Player.MainRectangle);
        }

        private void Draw(Graphics g)
        {
            DrawBackground(g);
            foreach (var key in _tileImages.Keys.Where(t=>t!=Tile.Dark))
                DrawTiles(g, key);
            DrawPlayer(g);
            DrawTray(g);
            DrawDark(g);

        }

        private void DrawDark(Graphics g)
        {
            var darkers = _game.Map.TileRectangles(Tile.Dark).ToArray();
            if (darkers.Length <= 0)
                return;
            foreach (var rectangle in darkers)
                g.DrawImage(_tileImages[Tile.Dark][0], rectangle);
        }

        private void DrawPlayer(Graphics g)
        {
            g.DrawImage(_playerImage,_game.Player.MainRectangle);
        }

        private void DrawBackground(Graphics g)
        {
            var brush = new LinearGradientBrush(ClientRectangle, Color.CornflowerBlue, Color.AliceBlue, LinearGradientMode.Horizontal);
            g.FillRectangle(brush, ClientRectangle);
            //DrawTilesDebug(g, Tile.Sharp, Color.Brown);
        }

        private void DrawTray(Graphics g)
        {            
            var trayCount = _game.Tray.Count;
            if (trayCount <= 1)
                return;
            foreach (var point in _game.Tray)
            {
                var rectangle = new Rectangle((point.ToVector() * Constants.TileSize).ToPoint(), Map.TileSize);
                rectangle.Inflate(Constants.IflateSize);
                g.DrawImage(_trayImage,rectangle);   
            }
            
        }

        private void DrawTiles(Graphics g, Tile tile)
        {
            var tiles = _tileArrays.Value[tile];
            var images = _tileImages[tile];
            if (tiles.Length <= 0)
                return;
            foreach (var rectangle in tiles)
            {
                var image = _game.Map.AnyOtherTiles(Tile.None, rectangle) || _game.Map.AnyOtherTiles(Tile.Dark,rectangle)
                                ? images[1]
                                : images[0];
                var rect = rectangle;
                rect.Inflate(Constants.IflateSize);
                g.DrawImage(image, rect);
            }
        }

        private void DrawTilesDebug(Graphics g, Tile tile, Color color)
        {
            var walls = _tileArrays.Value[tile];
            if (walls.Length > 0)
                g.FillRectangles(new LinearGradientBrush(ClientRectangle,color,Color.Blue, LinearGradientMode.Vertical), walls);
        }

        private void TimerTick(object sender, EventArgs e)
        {
            foreach (var key in _keysPressed)
                _keyMovements[key]();
            _game.OneIteration();
            
            Invalidate();
        }
    }
}
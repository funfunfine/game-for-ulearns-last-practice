﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YetAnotherAttemptToGame
{
    public class GameModel
    {
        private readonly int _deltaTime;

        public Player Player { get; private set; }

        public readonly Map Map;

        public GameState State { get; private set; } = GameState.InProcess;

        public readonly List<Point> Tray = new List<Point>();

        private readonly Player _startPlayer;

        public GameModel(int dt,Map map, Player player)
        {
            _deltaTime = dt;
            Map = map;
            _startPlayer = player.Copy();
            Player = player;
            Tray.Add(Player.MainRectangle.Location);
        }

        public void OneIteration()
        {
            if (Player.IsDead)
            {
                State = GameState.Lost;
                return;
            }

            if (Player.IsWinner)
            {
                State = GameState.Won;
                return;
            }
            Player.UpdatePosition(_deltaTime);
            Map.ExpandDarkness(Constants.DarknessSpeed);
            if (Player.IsOnGround)
                Tray.Add( Player.BottomIntersectedTiles.First().ToPoint());
        }

        public void MoveRight() => Player.MoveRight();

        public void MoveLeft() => Player.MoveLeft();

        public void Jump() => Player.Jump();

        public void Restart()
        {
            Player = _startPlayer.Copy();
            Tray.Clear();
            Tray.Add(Player.MainRectangle.Location);
            State = GameState.InProcess;
            Map.ClearDarkness();
        }

        public enum GameState
        {
            Won,Lost,InProcess
        }
    }
}

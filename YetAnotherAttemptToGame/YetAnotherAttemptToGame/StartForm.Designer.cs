﻿namespace YetAnotherAttemptToGame
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Game = new System.Windows.Forms.Button();
            this.Constructor = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.GameWidth = new System.Windows.Forms.TextBox();
            this.GameHeight = new System.Windows.Forms.TextBox();
            this.TileSize = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Levels = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Game
            // 
            this.Game.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Game.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.play;
            this.Game.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Game.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Game.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.Game.Location = new System.Drawing.Point(190, 442);
            this.Game.Name = "Game";
            this.Game.Size = new System.Drawing.Size(124, 95);
            this.Game.TabIndex = 0;
            this.Game.UseVisualStyleBackColor = false;
            this.Game.Click += new System.EventHandler(this.button1_Click);
            // 
            // Constructor
            // 
            this.Constructor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Constructor.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.create;
            this.Constructor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Constructor.FlatAppearance.BorderSize = 0;
            this.Constructor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Constructor.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Constructor.Location = new System.Drawing.Point(779, 429);
            this.Constructor.Name = "Constructor";
            this.Constructor.Size = new System.Drawing.Size(93, 51);
            this.Constructor.TabIndex = 1;
            this.Constructor.UseVisualStyleBackColor = false;
            this.Constructor.Click += new System.EventHandler(this.Constructor_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.rules;
            this.pictureBox1.Location = new System.Drawing.Point(156, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(802, 411);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // GameWidth
            // 
            this.GameWidth.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.GameWidth.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GameWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.3F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GameWidth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.GameWidth.Location = new System.Drawing.Point(752, 518);
            this.GameWidth.Margin = new System.Windows.Forms.Padding(0);
            this.GameWidth.Name = "GameWidth";
            this.GameWidth.Size = new System.Drawing.Size(42, 19);
            this.GameWidth.TabIndex = 3;
            this.GameWidth.Text = "1000";
            this.GameWidth.TextChanged += new System.EventHandler(this.Width_TextChanged);
            // 
            // GameHeight
            // 
            this.GameHeight.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.GameHeight.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GameHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.3F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GameHeight.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.GameHeight.Location = new System.Drawing.Point(813, 517);
            this.GameHeight.Margin = new System.Windows.Forms.Padding(0);
            this.GameHeight.Name = "GameHeight";
            this.GameHeight.Size = new System.Drawing.Size(47, 19);
            this.GameHeight.TabIndex = 4;
            this.GameHeight.Text = "1000";
            this.GameHeight.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // TileSize
            // 
            this.TileSize.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.TileSize.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TileSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.3F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TileSize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.TileSize.Location = new System.Drawing.Point(907, 517);
            this.TileSize.Margin = new System.Windows.Forms.Padding(0);
            this.TileSize.Name = "TileSize";
            this.TileSize.Size = new System.Drawing.Size(34, 19);
            this.TileSize.TabIndex = 5;
            this.TileSize.Text = "16";
            this.TileSize.TextChanged += new System.EventHandler(this.TileSize_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(749, 494);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Width";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(810, 494);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Height";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(904, 494);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Tilesize";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Levels
            // 
            this.Levels.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.Levels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Levels.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Levels.FormattingEnabled = true;
            this.Levels.Location = new System.Drawing.Point(370, 470);
            this.Levels.Name = "Levels";
            this.Levels.Size = new System.Drawing.Size(176, 21);
            this.Levels.TabIndex = 9;
            this.Levels.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.exit;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(23, 485);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(46, 51);
            this.button1.TabIndex = 10;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.BackgroundImage = global::YetAnotherAttemptToGame.Properties.Resources.name;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(970, 560);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Levels);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TileSize);
            this.Controls.Add(this.GameHeight);
            this.Controls.Add(this.GameWidth);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Constructor);
            this.Controls.Add(this.Game);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "StartForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Game;
        private System.Windows.Forms.Button Constructor;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox GameWidth;
        private System.Windows.Forms.TextBox GameHeight;
        private System.Windows.Forms.TextBox TileSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox Levels;
        private System.Windows.Forms.Button button1;
    }
}
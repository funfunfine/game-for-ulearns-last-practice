﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;

namespace YetAnotherAttemptToGame
{
    internal sealed class ConstructorForm : Form
    {
        public  static readonly string PathToLevels = Environment.CurrentDirectory + @"\..\..\Levels\";
        private Map _map;
        private int _drawWidth = 1;

        private readonly Timer _timer = new Timer { Interval = 15 };

        private bool _mouseIsDown;
        private ComboBox _addChoosingBox;

        private int PlayerSize => int.TryParse(_playerSize.Text, out var size) ? size : 20;

        private Dictionary<string, Action<Point>> _addedTypes;
        private TextBox _wideField;
        private TextBox _pathToSave;
        private TextBox _pathToLoadFrom;
        private TextBox _playerSize;

        private void InitializeWideField()
        {
            var label = new Label
            {
                Location = new Point(_map.Width + 25, 100),
                Text = @"Widht"
            };
            Controls.Add(label);
            _wideField = new TextBox
            {
                Location = new Point(_map.Width + 25, 125),
                Size = new Size(20, 20),
                Text = @"1"
            };
            _wideField.TextChanged += WideChange;
            Controls.Add(_wideField);
        }

        private void WideChange(object sender, EventArgs e)
        {
            if (int.TryParse(_wideField.Text, out var possibleResult) && possibleResult > 0 && possibleResult < 10)
                _drawWidth = possibleResult;
        }

        public ConstructorForm(int width, int height)
        {

            _map = new Map(width, height);
            _map.AddBorder();

            InitializeForm();
            InitializeChooser();
            InitializeSave();
            InitializeLoad();
            InitializeWideField();
            InitializePlayerSize();

            _timer.Start();
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
        }

        private void InitializeForm()
        {
            ClientSize = new Size(_map.Width + 250, _map.Height);
            FormBorderStyle = FormBorderStyle.Fixed3D;
            Paint += DrawField;
            DoubleBuffered = true;
            _timer.Tick += OnTick;
        }

        private void InitializeSave()
        {
            var saveButton = new Button
            {
                Location = new Point(ClientSize.Width - 250 + 25, 200),
                Size = new Size(60, 30),
                Text = @"Save"
            };
            saveButton.Click += SaveClick;
            Controls.Add(saveButton);

            _pathToSave = new TextBox
            {
                Location = new Point(ClientSize.Width - 250 + 25, 250),
                Size = new Size(90, 25),
                Text = @"testMap"
            };
            Controls.Add(_pathToSave);
        }

        private void InitializePlayerSize()
        {
            var playerSizeLabel = new Label
            {
                Location = new Point(ClientSize.Width - 250 + 25, 550),
                Text = @"Set Player size"
            };
            Controls.Add(playerSizeLabel);
            _playerSize = new TextBox
            {
                Location = new Point(ClientSize.Width - 250 + 25, 590),
                Text = @"20"
            };
            Controls.Add(_playerSize);
        }

        private void InitializeLoad()
        {
            var loadButton = new Button
            {
                Location = new Point(ClientSize.Width - 250 + 25, 400),
                Size = new Size(60, 30),
                Text = @"Load"
            };
            loadButton.Click += LoadClick;
            Controls.Add(loadButton);

            _pathToLoadFrom = new TextBox
            {
                Location = new Point(ClientSize.Width - 250 + 25, 450),
                Size = new Size(90, 25),
                Text = @"testMap"
            };
            Controls.Add(_pathToLoadFrom);
        }

        private void LoadClick(object sender, EventArgs e)
        {
            _map = BinarySerialization.ReadFromBinaryFile<Map>(PathToLevels +
                                                        _pathToLoadFrom.Text);
            _addedTypes = new Dictionary<string, Action<Point>>
            {
                ["Wall"] = _map.AddWall,
                ["Sharper"] = _map.AddSharper,
                ["Win Area"] = _map.AddWinArea,
                ["None"] = _map.AddNone,
                ["Dark"] = _map.AddDarkSeed,
                ["Player"] = location => _map.PlayerStartRectangle = new Rectangle((location.ToVector() * Constants.TileSize).ToPoint(), new Size(PlayerSize, PlayerSize))
            };
        }

        private void InitializeChooser()
        {
            var chooserLabel = new Label
            {
                Location = new Point(ClientSize.Width - 250 + 25, 25),
                Text = @"Type to add:"
            };
            Controls.Add(chooserLabel);

            _addedTypes = new Dictionary<string, Action<Point>>
            {
                ["Wall"] = _map.AddWall,
                ["Sharper"] = _map.AddSharper,
                ["Win Area"] = _map.AddWinArea,
                ["None"] = _map.AddNone,
                ["Dark"] = _map.AddDarkSeed,
                ["Player"] = location => _map.PlayerStartRectangle = new Rectangle((location.ToVector()*Constants.TileSize).ToPoint(), new Size(PlayerSize, PlayerSize))
            };

            _addChoosingBox = new ComboBox
            {
                Location = new Point(ClientSize.Width - 250 + 25, 50),
                Size = new Size(100, 30),
                DropDownStyle = ComboBoxStyle.DropDownList
            };

            foreach (var key in _addedTypes.Keys)
                _addChoosingBox.Items.Add(key);

            _addChoosingBox.SelectedIndex = 0;
            _addChoosingBox.Show();
            Controls.Add(_addChoosingBox);
        }


        private void SaveClick(object sender, EventArgs e)
        {
            BinarySerialization.WriteToBinaryFile(PathToLevels + _pathToSave.Text, _map);
        }

        private void OnTick(object sender, EventArgs e) => Invalidate();

        private void DrawField(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            g.DrawRectangles(Pens.Black, _map.AllTiles.ToArray());

            
            var walls = _map.TileRectangles(Tile.Wall).ToArray();
            var sharpers = _map.TileRectangles(Tile.Sharp).ToArray();
            var winArea = _map.TileRectangles(Tile.Win).ToArray();

            g.FillRectangle(Brushes.BlueViolet,_map.PlayerStartRectangle);

            if (sharpers.Length > 0)
                g.FillRectangles(Brushes.Red, sharpers);

            if (walls.Length > 0)
                g.FillRectangles(Brushes.ForestGreen, walls);

            if (winArea.Length > 0)
                g.FillRectangles(Brushes.Aqua, winArea);
            var darkers = _map.TileRectangles(Tile.Dark).ToArray();
            if (darkers.Length > 0)
                g.FillRectangles(Brushes.Black, darkers);


        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.B || e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                _mouseIsDown = true;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (!_mouseIsDown)
                return;
            var point = new Point(e.X, e.Y);
            if (!_map.InBounds(point))
                return;
            var tile = Map.ConvertToTiles(point.ToVector())
                          .ToPoint();
            foreach (var localPoint in _map.GetLocalTiles(tile, _drawWidth - 1).Append(tile))
                _addedTypes[(string)_addChoosingBox.SelectedItem](localPoint);
        }



        protected override void OnMouseUp(MouseEventArgs e) => _mouseIsDown = false;
    }
}